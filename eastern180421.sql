-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 21, 2022 at 08:33 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eastern180421`
--

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `id` int(11) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(100) DEFAULT NULL,
  `joining_date` varchar(100) NOT NULL,
  `login_date` date DEFAULT NULL,
  `logout_date` date DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1 = logged in',
  `eventname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(1, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021/04/16 14:01:25', '2021-04-16', '2021-04-16', 1, 'Abbott'),
(2, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021/04/17 10:35:38', '2021-04-17', '2021-04-17', 1, 'Abbott'),
(3, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021/04/17 20:09:17', '2021-04-17', '2021-04-17', 1, 'Abbott'),
(4, 'Shouvik Bhattacharya', 'shouvik.bhattacharya@abbott.com', 'Kolkata', 'AV', NULL, NULL, '2021/04/17 21:23:00', '2021-04-17', '2021-04-17', 1, 'Abbott'),
(5, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021/04/17 21:23:05', '2021-04-17', '2021-04-17', 1, 'Abbott'),
(6, 'Kuntal Bhattacharya', 'drkuntal@gmail.com', 'KOLKATA', 'RNTagore ', NULL, NULL, '2021/04/17 21:33:06', '2021-04-17', '2021-04-17', 1, 'Abbott'),
(7, 'sunil', 'sunil.roy@abbott.com', 'BBSR', 'A', NULL, NULL, '2021/04/17 21:36:59', '2021-04-17', '2021-04-17', 1, 'Abbott'),
(8, 'Suvro', 'drsuvrob@gmail.com', 'Kolkata', 'AGHL', NULL, NULL, '2021/04/17 21:38:27', '2021-04-17', '2021-04-17', 1, 'Abbott'),
(9, 'Udit', 'udit.mathur144@jtb-india.com', 'Test', 'Test', NULL, NULL, '2021/04/17 21:45:10', '2021-04-17', '2021-04-17', 1, 'Abbott'),
(10, 'Dr Aftab Khan', 'cardiokhan@gmail.com', 'Kolkata', 'Apollo Gleneagles ', NULL, NULL, '2021/04/18 17:23:18', '2021-04-18', '2021-04-18', 1, 'Abbott'),
(11, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021/04/18 17:59:56', '2021-04-18', '2021-04-18', 1, 'Abbott'),
(12, 'Kalyan Goswami', 'kalyan.goswami@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021/04/18 18:02:30', '2021-04-18', '2021-04-18', 1, 'Abbott'),
(13, 'Shouvik Bhattacharya', 'shouvik.bhattacharya@abbott.com', 'Kolkata', 'AV', NULL, NULL, '2021/04/18 18:15:52', '2021-04-18', '2021-04-18', 1, 'Abbott'),
(14, 'Subhabrata', 'subhabrata.banerjee@abbott.com', 'kolkata', 'av', NULL, NULL, '2021/04/18 18:24:11', '2021-04-18', '2021-04-18', 1, 'Abbott'),
(15, 'Sunil Roy', 'sunil.roy@abbott.com', 'bbsr', 'A', NULL, NULL, '2021/04/18 18:33:03', '2021-04-18', '2021-04-18', 1, 'Abbott'),
(16, 'Dr P K Sahoo', 'drprasant_s@apollohospitals.com', 'Bhubaneswar', 'Apollo Hospital', NULL, NULL, '2021/04/18 18:35:08', '2021-04-18', '2021-04-18', 1, 'Abbott'),
(17, 'Dr P K Sahoo', 'drprasant_s@apollohospitals.com', 'Bhubaneswar', 'Apollo Hospital', NULL, NULL, '2021/04/18 18:39:27', '2021-04-18', '2021-04-18', 1, 'Abbott'),
(18, 'Dr Debdatta Bhattacharyya', 'dev.heartline@gmail.com', 'kolkata', 'NH-RTIICS', NULL, NULL, '2021/04/18 18:48:20', '2021-04-18', '2021-04-18', 1, 'Abbott'),
(19, 'Dr Aftab Khan', 'cardiokhan@gmail.com', 'Kolkata', 'Apollo Gleneagles ', NULL, NULL, '2021/04/18 18:56:08', '2021-04-18', '2021-04-18', 1, 'Abbott'),
(20, 'dr p k hazra', 'phpkhazra@gmail.com', 'KOLKATA', 'AMRI', NULL, NULL, '2021/04/18 19:00:49', '2021-04-18', '2021-04-18', 1, 'Abbott'),
(21, 'Suvro Banerjee', 'drsuvrob@gmail.com', 'Kolkata', 'AGHL', NULL, NULL, '2021/04/18 19:05:03', '2021-04-18', '2021-04-18', 1, 'Abbott'),
(22, 'Kuntal Bhattacharya', 'drkuntal@gmail.com', 'KOLKATA', 'RNTagore ', NULL, NULL, '2021/04/18 19:08:26', '2021-04-18', '2021-04-18', 1, 'Abbott'),
(23, 'p k hazra', 'prakashhazra@hotmail.com', 'calcutta', 'amri', NULL, NULL, '2021/04/18 19:10:50', '2021-04-18', '2021-04-18', 1, 'Abbott'),
(24, 'Kalyan Goswami', 'kalyan.goswami@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021/04/18 21:00:33', '2021-04-18', '2021-04-18', 1, 'Abbott');

-- --------------------------------------------------------

--
-- Table structure for table `faculty_new`
--

CREATE TABLE `faculty_new` (
  `id` int(50) NOT NULL,
  `Name` varchar(200) NOT NULL,
  `EmailID` varchar(200) NOT NULL,
  `City` varchar(200) NOT NULL,
  `Hospital` varchar(200) NOT NULL,
  `Last Login On` varchar(200) NOT NULL,
  `Logout Times` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_email`, `user_question`, `asked_at`, `eventname`, `speaker`, `answered`) VALUES
(1, 'Dr Basabendra Chowdhury', 'cbasabendra@gmail.com', 'Should RFR be considered as standard procedure for MVD ?\r\n', '2021-04-18 20:39:03', 'Abbott', 0, 0),
(2, 'Dr Sidath Singh', 'sidnathpmch@gmail.com', 'Any precautions should be taken for OCT in MVD \r\n\r\n', '2021-04-18 20:44:49', 'Abbott', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL,
  `token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`, `token`) VALUES
(1, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-04-16 13:59:36', '2021-04-16 13:59:36', '2021-04-16 15:29:36', 0, 'Abbott', 'b5c60ac2955172382e8dd52ab04e059c'),
(2, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-04-16 13:59:36', '2021-04-16 13:59:36', '2021-04-16 13:59:56', 0, 'Abbott', 'dbbb6bac0837ce9d643fa73c51936c3e'),
(3, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-04-16 14:16:03', '2021-04-16 14:16:03', '2021-04-16 14:17:42', 0, 'Abbott', '7c47eea02387eeba65f5d79552d0e82e'),
(4, 'Kalyan Goswami', 'Kalyan.goswami@abbott.com', 'Bangalore ', 'Av', NULL, NULL, '2021-04-16 16:01:48', '2021-04-16 16:01:48', '2021-04-16 17:31:48', 0, 'Abbott', '289ba88bc85c0aebe04135705c40f0b5'),
(5, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-04-17 10:32:10', '2021-04-17 10:32:10', '2021-04-17 10:32:16', 0, 'Abbott', '8fd87cc89f6639335cac0b58e7c4bd84'),
(6, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-04-17 20:02:24', '2021-04-17 20:02:24', '2021-04-17 20:04:43', 0, 'Abbott', '21cc0043c60293baf89b362def09a3fa'),
(7, 'Suvro', 'drsuvrob@gmail.com', 'Kolkata', 'AGHL', NULL, NULL, '2021-04-17 21:35:20', '2021-04-17 21:35:20', '2021-04-17 23:05:20', 0, 'Abbott', '9345f2bfd03825697c6a3ae5ad25eb05'),
(8, 'Amitabha Ray', 'amitabha@saimed.in', 'Kolkata ', 'Saimed', NULL, NULL, '2021-04-18 17:27:46', '2021-04-18 17:27:46', '2021-04-18 18:57:46', 0, 'Abbott', 'ee36f6b4f78ceaeaa4409f82f1a6b512'),
(9, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-04-18 18:10:18', '2021-04-18 18:10:18', '2021-04-18 19:40:18', 0, 'Abbott', 'c8188610659a3df8b3ecb384b6a9dfce'),
(10, 'V', 'viraj@coact.co.in', 'Pune', 'NH', NULL, NULL, '2021-04-18 18:14:14', '2021-04-18 18:14:14', '2021-04-18 19:44:14', 0, 'Abbott', '4163716b1fda9522e839eceff8ada25e'),
(11, 'Udit', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-04-18 18:17:23', '2021-04-18 18:17:23', '2021-04-18 18:24:30', 0, 'Abbott', '5bf3d15d0c69f9763d818029fef1a8ee'),
(12, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-04-18 18:19:14', '2021-04-18 18:19:14', '2021-04-18 19:49:14', 0, 'Abbott', 'd7f8cdc3498b6476596b046253dacc19'),
(13, 'Rahul kolhekar ', 'rahul.vkolhekar@abbott.com', 'Kolkata ', 'AV', NULL, NULL, '2021-04-18 18:58:23', '2021-04-18 18:58:23', '2021-04-18 20:28:23', 0, 'Abbott', '1c78087575f4219d8a62df3ecaf199f4'),
(14, 'Atanu Banerjee ', 'atanu.banerjee@abbott.com', 'Kolkata', 'AV', NULL, NULL, '2021-04-18 19:01:18', '2021-04-18 19:01:18', '2021-04-18 20:31:18', 0, 'Abbott', '2bbdf0ff5402536432b7f2ac995c3d8e'),
(15, 'Sunil Roy', 'sunil.roy@abbott.com', 'Bhubaneswar', 'AV', NULL, NULL, '2021-04-18 19:02:22', '2021-04-18 19:02:22', '2021-04-18 19:19:04', 0, 'Abbott', '58f9a3ced45d8c8138a7e4880befaa4b'),
(16, 'Siddharth Mani	', 'siddhartha.mani84@gmail.com', 'RTIICS', 'Kolkata', NULL, NULL, '2021-04-18 19:04:25', '2021-04-18 19:04:25', '2021-04-18 20:34:25', 0, 'Abbott', 'f62d6eab3aa86237d756175b54a0ea4f'),
(17, 'Dr Rituparna Baruah', 'dr.rituparna@rediffmail.com', 'Guwahati', 'International Hospital', NULL, NULL, '2021-04-18 19:04:37', '2021-04-18 19:04:37', '2021-04-18 20:34:37', 0, 'Abbott', '93416f8eb3bc6c68eb3e7bff832dfc9f'),
(18, 'Suman Kumar Saha', 'sumankumarsaha360@gmail.com', 'Kolkata', 'R.N.Tagore Hospital', NULL, NULL, '2021-04-18 19:06:00', '2021-04-18 19:06:00', '2021-04-18 20:36:00', 0, 'Abbott', 'c57210902a0ab6c4c0d92e9ec10575ed'),
(19, 'Dr Shashank Baruah ', 'shashank.baruah@yahoo.com', 'Guwahati', 'Healthcity Hospital', NULL, NULL, '2021-04-18 19:06:02', '2021-04-18 19:06:02', '2021-04-18 20:36:02', 0, 'Abbott', 'a954c5e515ba48d2fb5a207f18040431'),
(20, 'Suman Kumar Saha', 'sumankumarsaha360@gmail.com', 'Kolkata', 'R.N.Tagore Hospital', NULL, NULL, '2021-04-18 19:06:24', '2021-04-18 19:06:24', '2021-04-18 20:36:24', 0, 'Abbott', '9bbcdaa6c85eec22f9ce2ee00bc53c3f'),
(21, 'Subhabrata Banerjee', 'subhabrata.banerjee@abbott.com', 'Kolkata ', 'AV ', NULL, NULL, '2021-04-18 19:06:44', '2021-04-18 19:06:44', '2021-04-18 20:36:44', 0, 'Abbott', '0585da058cf4297200dc901b596d0a73'),
(22, 'Dr K C Narzary', 'drkrishna_narzary@yahoo.co.in', 'Guwahati', 'GNRC', NULL, NULL, '2021-04-18 19:06:58', '2021-04-18 19:06:58', '2021-04-18 20:36:58', 0, 'Abbott', '4100e6dd8ce03fe0193e4f3fc51398e8'),
(23, 'Rahul Kolhekar ', 'rahul.vkolhekar@abbott.com', 'Kolkata ', 'AV', NULL, NULL, '2021-04-18 19:07:24', '2021-04-18 19:07:24', '2021-04-18 20:37:24', 0, 'Abbott', 'a6ad149ce681cf2374629aea03905485'),
(24, 'Dr Chandan Modak', 'chandan2123@yahoo.com', 'Guwahati', 'Dispur Hospital', NULL, NULL, '2021-04-18 19:07:34', '2021-04-18 19:07:34', '2021-04-18 20:37:34', 0, 'Abbott', '22ba958eaece170d4fb57fd4323a12af'),
(25, 'Dr Rajesh Das ', 'rajesh.das213@gmail.com', 'Guwahati', 'NEMCARE Hospital', NULL, NULL, '2021-04-18 19:07:37', '2021-04-18 19:07:37', '2021-04-18 20:37:37', 0, 'Abbott', 'ea9f5777ab26cde69f3530c5fb331e8d'),
(26, 'Kalyan Goswami', 'Kalyan.goswami@abbott.com', 'Bangalore ', 'AV ', NULL, NULL, '2021-04-18 19:08:40', '2021-04-18 19:08:40', '2021-04-18 20:38:40', 0, 'Abbott', '480641e5101d59d8fda528de22f99987'),
(27, 'Dr P J Bhattacharya', 'pjbhatta2123@gmail.com', 'Guwahati', 'GMCH', NULL, NULL, '2021-04-18 19:09:41', '2021-04-18 19:09:41', '2021-04-18 20:39:41', 0, 'Abbott', 'a7f8c95db1119c08adbe4b72528be3ff'),
(28, 'Raju Rao', 'rajurao3312@gmail.com', 'Guwahati', 'International Hospital', NULL, NULL, '2021-04-18 19:10:07', '2021-04-18 19:10:07', '2021-04-18 20:40:07', 0, 'Abbott', '4d60161d6bfe77eb7db6b1794af9a6b9'),
(29, 'Banjeet Singh', 'banjeet@rediffmail.com', 'Guwahati', 'International Hospital', NULL, NULL, '2021-04-18 19:10:15', '2021-04-18 19:10:15', '2021-04-18 20:40:15', 0, 'Abbott', '44fe15dc7ce75188696a1eaf46d7502f'),
(30, 'Suman Kumar Saha', 'sumankumarsaha360@gmail.com', 'Kolkata', 'R.N.Tagore Hospital', NULL, NULL, '2021-04-18 19:10:26', '2021-04-18 19:10:26', '2021-04-18 20:40:26', 0, 'Abbott', '1ac5aef6ff67e24bbbf85b37eb74972d'),
(31, 'Debabrata Roy	', 'debroy68@ymail.com', 'Kolkata', 'RTIICS', NULL, NULL, '2021-04-18 19:10:45', '2021-04-18 19:10:45', '2021-04-18 20:40:45', 0, 'Abbott', '579688ddfaf5e02447df71e49fa45255'),
(32, 'Enem Horo', 'henem45678@gmail.com', 'Guwahati', 'GNRC', NULL, NULL, '2021-04-18 19:11:00', '2021-04-18 19:11:00', '2021-04-18 20:41:00', 0, 'Abbott', '502dc45aec0eda29184704fbf2ba2f9f'),
(33, 'Pankaj Sharma', 'sharmapankaj567@gmail.com', 'Guwahati', 'Healthcity Hospital', NULL, NULL, '2021-04-18 19:11:27', '2021-04-18 19:11:27', '2021-04-18 20:41:27', 0, 'Abbott', 'f801f480383c028331fadcfaa9922dcb'),
(34, 'Himaku Deka', 'dhimanku@yahoo.com', 'Guwahati', 'GNRC', NULL, NULL, '2021-04-18 19:11:39', '2021-04-18 19:11:39', '2021-04-18 20:41:39', 0, 'Abbott', 'eff9ff7a64e6b8a0b2aa9f02c768be2a'),
(35, 'Suman ', 'Laha.suman@gmail.com', 'Kolkata ', 'Rti', NULL, NULL, '2021-04-18 19:11:54', '2021-04-18 19:11:54', '2021-04-18 19:16:17', 0, 'Abbott', '1baddd6eaf29eca551bb52ec0f31bd20'),
(36, 'Mintu Baruah', 'mintubar223@yahoo.com', 'Guwahati', 'NEMCARE Hospital', NULL, NULL, '2021-04-18 19:12:01', '2021-04-18 19:12:01', '2021-04-18 20:42:01', 0, 'Abbott', '97f4d186943f433f19ae7e09527da6c3'),
(37, 'Ratheesh Kumar J	', 'ratheeshkj45@yahoo.com', 'Kolkata', 'Command Hospital', NULL, NULL, '2021-04-18 19:12:12', '2021-04-18 19:12:12', '2021-04-18 20:42:12', 0, 'Abbott', 'beeb487d9470e3f09b50eb34d60868b9'),
(38, 'Dr R L Bhanja', 'drrajiev@gmail.com', 'Bilaspur', 'Apollo', NULL, NULL, '2021-04-18 19:12:37', '2021-04-18 19:12:37', '2021-04-18 20:42:37', 0, 'Abbott', 'ba17e9d0aff7c7aa326168dac6912d14'),
(39, 'Munish Sharma	', 'munishsharma56@outlook.com', 'Kolkata', 'Command Hospital', NULL, NULL, '2021-04-18 19:13:19', '2021-04-18 19:13:19', '2021-04-18 20:43:19', 0, 'Abbott', '4a47ce0af758d9a3d6828818972f47a5'),
(40, 'Koushik Dasgupta', 'KOUSHIK_NMC@YAHOO.COM', 'KOLKATA', 'nh rtiics', NULL, NULL, '2021-04-18 19:14:12', '2021-04-18 19:14:12', '2021-04-18 20:44:12', 0, 'Abbott', '37fd493862d3290ff3aceb7cd070de38'),
(41, 'DP Rai	', 'dprai2011caedio@gmail.com', 'Kolkata', 'stnm', NULL, NULL, '2021-04-18 19:14:18', '2021-04-18 19:14:18', '2021-04-18 19:16:47', 0, 'Abbott', '566a7fc6550be9de96b6b02cb6b6e0ba'),
(42, 'Snehil Goswami', 'snehil.1508@gmail.com', 'Raipur', 'NH MMI RAIPUR', NULL, NULL, '2021-04-18 19:15:03', '2021-04-18 19:15:03', '2021-04-18 20:45:03', 0, 'Abbott', '3ff63d3dd9a4f840ac95b58090e58723'),
(43, 'Suman Kumar Saha', 'sumankumarsaha360@gmail.com', 'Kolkata', 'R.N.Tagore Hospital', NULL, NULL, '2021-04-18 19:16:32', '2021-04-18 19:16:32', '2021-04-18 19:23:10', 0, 'Abbott', 'ead433e00c46415ce37a171bdbbf6216'),
(44, 'partha paatim', 'Partha.partim@gmail.com', 'Jamshedpur ', 'Meditrina', NULL, NULL, '2021-04-18 19:16:57', '2021-04-18 19:16:57', '2021-04-18 19:18:10', 0, 'Abbott', 'a03ef4fe604222e196089d3d074d0be6'),
(45, 'Uttam Kumar Ojha', 'uttamojha.ctc@gmail.com', 'Bhubaneswar', 'Apollo hospitals', NULL, NULL, '2021-04-18 19:17:39', '2021-04-18 19:17:39', '2021-04-18 20:47:39', 0, 'Abbott', '3400464b9951a0ddb7d33c861ce587e1'),
(46, 'Akashya  Bhabe	', 'akabay1978@gmail.com', 'Kolkata', 'BNH HOSPITAL', NULL, NULL, '2021-04-18 19:17:57', '2021-04-18 19:17:57', '2021-04-18 20:47:57', 0, 'Abbott', '0fd08db4f4b926ad91a0c9525b5ab734'),
(47, 'Sunil Roy', 'sunil.roy@abbott.com', 'Bhubaneswar', 'AV', NULL, NULL, '2021-04-18 19:18:45', '2021-04-18 19:18:45', '2021-04-18 20:48:45', 0, 'Abbott', '4b23f25d6302c241efdaf28e76932ca9'),
(48, 'Majumdar', 'drmajundar@yahoo.com', 'Kolkata ', 'Rti', NULL, NULL, '2021-04-18 19:18:55', '2021-04-18 19:18:55', '2021-04-18 19:27:04', 0, 'Abbott', 'd5e564f99e1f780cddca2ac0f86e8d5e'),
(49, 'A . Agarwal	', 'ajaykumar.agarwal86@mhidhanbad.in', 'Kolkata', 'BNH HOSPITAL', NULL, NULL, '2021-04-18 19:20:08', '2021-04-18 19:20:08', '2021-04-18 20:50:08', 0, 'Abbott', '50adfec0b1cc4edb45d90a3d0b2dee40'),
(50, 'Dr Avik Karak', 'dravik@gmail.com', 'Kolkata', 'Calcutta Medical College ', NULL, NULL, '2021-04-18 19:22:41', '2021-04-18 19:22:41', '2021-04-18 20:52:41', 0, 'Abbott', '918d0483edd22478fe782b5e1d38cf12'),
(51, 'Dr Avik Karak', 'dravik@gmail.com', 'Kolkata', 'Calcutta Medical College ', NULL, NULL, '2021-04-18 19:22:41', '2021-04-18 19:22:41', '2021-04-18 20:52:41', 0, 'Abbott', 'd72e13698591b52f3fe1b28b22929fd8'),
(52, 'Dr Anindya Mukherjee', 'anindya768@yahoo.co.in', 'Kolkata', 'R G Kar Hospital', NULL, NULL, '2021-04-18 19:22:59', '2021-04-18 19:22:59', '2021-04-18 20:52:59', 0, 'Abbott', 'fe4cca8d3a74875d779b0163827e08a6'),
(53, 'Dr Basabendra Chowdhury', 'cbasabendra@gmail.com', 'Kolkata', 'Calcutta Medical College ', NULL, NULL, '2021-04-18 19:23:16', '2021-04-18 19:23:16', '2021-04-18 20:53:16', 0, 'Abbott', '52a5e7990f02de443660e9a3b7f5dc6b'),
(54, 'Samir Nayak', 'samirnayak703@gmail.com', 'BBSR', 'Apolo', NULL, NULL, '2021-04-18 19:23:33', '2021-04-18 19:23:33', '2021-04-18 20:53:33', 0, 'Abbott', '1a77a9edb562ea118eaa779a25129dcb'),
(55, 'Dr Mita Bar', 'mita.bar.87@gmail.com', 'Kolkata', 'Calcutta Medical College ', NULL, NULL, '2021-04-18 19:23:34', '2021-04-18 19:23:34', '2021-04-18 20:53:34', 0, 'Abbott', '8c45f2d4b1523ec5ee4380d0c1bd1c43'),
(56, 'Dr Bidyut Roy', 'bidyutroy451@gmail.com', 'Kolkata', 'SSKM', NULL, NULL, '2021-04-18 19:23:48', '2021-04-18 19:23:48', '2021-04-18 20:53:48', 0, 'Abbott', '79a1db4b49dab883fc83c1eaef4236f5'),
(57, 'Suman Kumar Saha', 'sumankumarsaha360@gmail.com', 'Kolkata', 'R.N.Tagore Hospital', NULL, NULL, '2021-04-18 19:24:11', '2021-04-18 19:24:11', '2021-04-18 20:54:11', 0, 'Abbott', '11822f9953de43bab365b41a1c4f59f2'),
(58, 'Dr Subir Sen', 'rits2subir@gmail.com', 'Kolkata', 'NRS Medical College ', NULL, NULL, '2021-04-18 19:24:14', '2021-04-18 19:24:14', '2021-04-18 20:54:14', 0, 'Abbott', '7e8f00ee82feec4b0409b961e2840ee9'),
(59, 'Tapan Kumar	', 'dr-_tapankumar78@rediffmail.com', 'Kolkata', 'Tata Main Hospital', NULL, NULL, '2021-04-18 19:24:23', '2021-04-18 19:24:23', '2021-04-18 20:54:23', 0, 'Abbott', '1b0ef081966626adc866646ec1f51dd7'),
(60, 'Ashok', 'sahooashok84@yahoo.in', 'BBSR', 'Amri', NULL, NULL, '2021-04-18 19:24:29', '2021-04-18 19:24:29', '2021-04-18 20:54:29', 0, 'Abbott', 'a32c324adbffdac5e6560f89d4e4dcfa'),
(61, 'Dr Suvendu Chatterjee', 'suvendu.blues@gmail.com', 'Kolkata', 'SSKM', NULL, NULL, '2021-04-18 19:24:38', '2021-04-18 19:24:38', '2021-04-18 20:54:38', 0, 'Abbott', 'd07ad9772cf2cf1045d08f666f9805fe'),
(62, 'Dr Anirban Das', 'nirvandoc@gmail.com', 'Kolkata', 'R G Kar Hospital', NULL, NULL, '2021-04-18 19:25:01', '2021-04-18 19:25:01', '2021-04-18 20:55:01', 0, 'Abbott', '8721a585669c1f762417dd10807dadd3'),
(63, 'Dr Arif Nabi', 'drarif.nabi@yahoo.co.in', 'Kolkata', 'R G Kar Hospital', NULL, NULL, '2021-04-18 19:25:24', '2021-04-18 19:25:24', '2021-04-18 20:55:24', 0, 'Abbott', 'aacf35e4f125a34a4afb955ae960de82'),
(64, 'Nadeem Afroz	', 'dr-nadeem56@live.com', 'Kolkata', 'The Mission Hospital', NULL, NULL, '2021-04-18 19:25:58', '2021-04-18 19:25:58', '2021-04-18 20:55:58', 0, 'Abbott', 'a3de48844688774455e9edf36b9ceaff'),
(65, 'Dr Dhananjay Kumar', 'djvishainya@gmail.com', 'Kolkata', 'Calcutta Medical College ', NULL, NULL, '2021-04-18 19:26:04', '2021-04-18 19:26:04', '2021-04-18 20:56:04', 0, 'Abbott', '441070317c71a850075fcb6d38d7559c'),
(66, 'Dr Kamlesh Ghorai', 'kamleshghorai@gmail.com', 'Kolkata', 'NRS Medical College ', NULL, NULL, '2021-04-18 19:26:25', '2021-04-18 19:26:25', '2021-04-18 20:56:25', 0, 'Abbott', 'b6c2fd000bd8afb52abb6ba4abd30aa0'),
(67, 'Dr Aditya Bhaskar', 'adityabhaskar445@gmail.com', 'Guwahati', 'Healthcity Hospital', NULL, NULL, '2021-04-18 19:26:50', '2021-04-18 19:26:50', '2021-04-18 20:56:50', 0, 'Abbott', 'a44abfaef0dd23309d98092975be17a7'),
(68, 'Munna Das	', 'munna.das.dr32@nhhospitals.org', 'Kolkata', 'NH ,West Bank', NULL, NULL, '2021-04-18 19:26:56', '2021-04-18 19:26:56', '2021-04-18 20:56:56', 0, 'Abbott', '5c1b5729ba5cf8b8a0e637f5c756895e'),
(69, 'Dr Abhay Chaturvedi', 'abhaybsmc@gmail.com', 'Kolkata', 'SSKM', NULL, NULL, '2021-04-18 19:27:12', '2021-04-18 19:27:12', '2021-04-18 20:57:12', 0, 'Abbott', '36ed761063309e68dcc0ae79ac29f7e9'),
(70, 'Dr Ankit Gupta', 'drankitgupta2017@gmail.com', 'Kolkata', 'R G Kar Hospital', NULL, NULL, '2021-04-18 19:27:36', '2021-04-18 19:27:36', '2021-04-18 20:57:36', 0, 'Abbott', '85c936a2632fccededa70418bd56b240'),
(71, 'Dr Abhirup Sinha', 'dr.abhirup.sinha@gmail.com', 'Kolkata', 'SSKM', NULL, NULL, '2021-04-18 19:27:56', '2021-04-18 19:27:56', '2021-04-18 20:57:56', 0, 'Abbott', 'b1aa6667c29c0692bc7256f32650cd18'),
(72, 'Mandar saha', 'mandar.saha@gmail.com', 'Jamshedpur', 'Tata', NULL, NULL, '2021-04-18 19:28:05', '2021-04-18 19:28:05', '2021-04-18 20:04:02', 0, 'Abbott', 'ad6140995ccd61880f5e551ad3c015e2'),
(73, 'Dr Aninda Sarkar', 'consult.cardio@gmail.com', 'Kolkata', 'NRS Medical College ', NULL, NULL, '2021-04-18 19:28:18', '2021-04-18 19:28:18', '2021-04-18 20:58:18', 0, 'Abbott', 'a5955ff9186a23ef88d1784e910f06a1'),
(74, 'Dr Kuntal Bhattacharya', 'kuntibhai@gmail.com', 'Kolkata', 'Calcutta Medical College ', NULL, NULL, '2021-04-18 19:29:27', '2021-04-18 19:29:27', '2021-04-18 20:59:27', 0, 'Abbott', '3a68ab3005d0dc726251585e5652bf93'),
(75, 'Dr Kaushik Manna', 'drkaushikmanna@gmail.com', 'Kolkata', 'R G Kar Hospital', NULL, NULL, '2021-04-18 19:29:44', '2021-04-18 19:29:44', '2021-04-18 20:59:44', 0, 'Abbott', '773f794b6e3a66a0b77434ae46c6d704'),
(76, 'Dr Sebabrata Jana', 'janasebabrata@yahoo.com', 'Kolkata', 'SSKM', NULL, NULL, '2021-04-18 19:31:18', '2021-04-18 19:31:18', '2021-04-18 21:01:18', 0, 'Abbott', '5212de1bde82b90b0cbc972240258f05'),
(77, 'Dr Souvik Ray', 'drsouvikray913@gmail.com', 'Kolkata', 'R G Kar Hospital', NULL, NULL, '2021-04-18 19:31:34', '2021-04-18 19:31:34', '2021-04-18 21:01:34', 0, 'Abbott', '5aa6d0997a197820643ee7a2652162c0'),
(78, 'Dr Sidath Singh', 'sidnathpmch@gmail.com', 'Kolkata', 'NRS Medical College ', NULL, NULL, '2021-04-18 19:31:54', '2021-04-18 19:31:54', '2021-04-18 21:01:54', 0, 'Abbott', '3d56094f07cfa3aca1bc11bd57a01ccb'),
(79, 'Dr Praveen Shukla', 'drpshukla@gmail.com', 'Kolkata', 'SSKM', NULL, NULL, '2021-04-18 19:32:13', '2021-04-18 19:32:13', '2021-04-18 21:02:13', 0, 'Abbott', '2145b07353ded7bcdef6428c5c3f9d74'),
(80, 'Amit Bhauwala	', 'bhauwala33@yahoo.com', 'Kolkata', 'AMRI Mukundapur', NULL, NULL, '2021-04-18 19:33:07', '2021-04-18 19:33:07', '2021-04-18 21:03:07', 0, 'Abbott', '5460b02ce1fec4bbee086432b24c9c3a'),
(81, 'Dr Nasim Mondal', 'nasims24@gmail.com', 'Kolkata', 'SSKM', NULL, NULL, '2021-04-18 19:33:56', '2021-04-18 19:33:56', '2021-04-18 21:03:56', 0, 'Abbott', 'a6cc3a074a90bbab88500a42f7e0c62c'),
(82, 'Munish Sharma	', 'munishsharma59@outlook.com', 'Kolkata', 'Command Hospital', NULL, NULL, '2021-04-18 19:35:35', '2021-04-18 19:35:35', '2021-04-18 21:05:35', 0, 'Abbott', 'f18665d99488bdb9c97defbb1753b0d8'),
(83, 'Dr Arijit Ghosh ', 'arijitjoy@gmail.com', 'Kolkata', 'AMRI Salt Lake ', NULL, NULL, '2021-04-18 19:36:18', '2021-04-18 19:36:18', '2021-04-18 21:06:18', 0, 'Abbott', '2935461fdbf7d5a1ed40db24846e7137'),
(84, 'Dr Aritra Konar ', 'konar1978.dr@gmail.com', 'Kolkata', 'Apollo Gleneagles ', NULL, NULL, '2021-04-18 19:36:40', '2021-04-18 19:36:40', '2021-04-18 21:06:40', 0, 'Abbott', '3a66a3f305116ed651096ee5af887ab0'),
(85, 'Dr Jayanta Saha', 'drjayanta.saha2010@gmail.com', 'Kolkata', 'Calcutta Medical College ', NULL, NULL, '2021-04-18 19:36:51', '2021-04-18 19:36:51', '2021-04-18 21:06:51', 0, 'Abbott', '5e72675a1dd09d49729640099d7aaae4'),
(86, 'Dr Asfaque Ahmed ', 'ahmed.asfaque@gmail.com', 'Kolkata', 'Apollo Hospital Kolkata', NULL, NULL, '2021-04-18 19:37:13', '2021-04-18 19:37:13', '2021-04-18 21:07:13', 0, 'Abbott', 'b67718ce1408cb933d40b62b3277d9c7'),
(87, 'Dr Nabarun Roy', 'nabarun.roy@gmail.com', 'Kolkata', 'Apollo Hospital Kolkata', NULL, NULL, '2021-04-18 19:37:40', '2021-04-18 19:37:40', '2021-04-18 21:07:40', 0, 'Abbott', 'de0cde08f2aa659e1d1bcf9c07b51e9d'),
(88, 'Dr Asraful Haque ', 'haque.asraful@gmail.com', 'Kolkata', 'S S Chatterjee Hospital ', NULL, NULL, '2021-04-18 19:38:06', '2021-04-18 19:38:06', '2021-04-18 21:08:06', 0, 'Abbott', 'e422846b88f3827774d3aa27e117bc21'),
(89, 'Soumya Patra	', 'soumya.patra89@medicasynergie.in', 'Kolkata', 'Medica Hospital', NULL, NULL, '2021-04-18 19:38:13', '2021-04-18 19:38:13', '2021-04-18 21:08:13', 0, 'Abbott', 'a1879cdc67bfa7f06cac97f88751ef8d'),
(90, 'Dr Raja Nag ', 'drrajanag@gmail.com', 'Kolkata', 'Apollo Hospital Kolkata', NULL, NULL, '2021-04-18 19:38:39', '2021-04-18 19:38:39', '2021-04-18 21:08:39', 0, 'Abbott', 'f9a8b969d6817a31db3876dccadab442'),
(91, 'Dr Debasis Ghosh ', 'ghosh.deba@gmail.com', 'Kolkata', 'Apollo Kolkata ', NULL, NULL, '2021-04-18 19:39:16', '2021-04-18 19:39:16', '2021-04-18 21:09:16', 0, 'Abbott', '2755de06369a91f1f9b5d567d5b1c1f6'),
(92, 'Sunandan Sikdar	', 'sunandansikdar11@gmail.com', 'Kolkata', 'NH Barasat', NULL, NULL, '2021-04-18 19:39:34', '2021-04-18 19:39:34', '2021-04-18 21:09:34', 0, 'Abbott', 'fbe9da3195e0cd0c1f6ce44ff43b331f'),
(93, 'Bala', 'bala.subramani@gmail.com', 'Kolkata', 'Apollo', NULL, NULL, '2021-04-18 19:39:35', '2021-04-18 19:39:35', '2021-04-18 21:09:35', 0, 'Abbott', '32bd7a23ecee73d590ac620cca5af487'),
(94, 'Dr Subhashis Roychowdhury ', 'drsubhasis.roychowdhury@yahoo.com', 'Kolkata', 'AMRI Salt Lake ', NULL, NULL, '2021-04-18 19:39:58', '2021-04-18 19:39:58', '2021-04-18 21:09:58', 0, 'Abbott', '6d80adb2c30f1a5330c54b6108e3c9ae'),
(95, 'Dr Asit Das', 'dradascard@rediffmail.com', 'Kolkata', 'SSKM', NULL, NULL, '2021-04-18 19:40:06', '2021-04-18 19:40:06', '2021-04-18 21:10:06', 0, 'Abbott', '179374de9ff8d872e4bc38704086cee4'),
(96, 'Dr Sunip Banerjee ', 'sgcardiaccare@gmail.com', 'Kolkata', 'SG', NULL, NULL, '2021-04-18 19:40:29', '2021-04-18 19:40:29', '2021-04-18 21:10:29', 0, 'Abbott', 'bcb53ff008b4d5a1f703de524b385f8a'),
(97, 'Dr Saroj Mondal ', 'drsarojkumarr@gmail.com', 'Kolkata', 'SSKM', NULL, NULL, '2021-04-18 19:40:33', '2021-04-18 19:40:33', '2021-04-18 21:10:33', 0, 'Abbott', '75588b6065401d8259f78a4f82fce8e8'),
(98, 'Dr Sachit Majumder ', 'Sachit.majumder@gmail.com', 'Kolkata', 'Apollo Gleneagles ', NULL, NULL, '2021-04-18 19:41:05', '2021-04-18 19:41:05', '2021-04-18 21:11:05', 0, 'Abbott', '1260449078ddbdfc5f93ff0165278459'),
(99, 'Dr Bikash Majumder ', 'drbikashmaj@yahoo.com', 'Kolkata', 'Apollo Gleneagles ', NULL, NULL, '2021-04-18 19:41:48', '2021-04-18 19:41:48', '2021-04-18 21:11:48', 0, 'Abbott', '124cfd154ff1c9d6785ccfac3efe1a37'),
(100, 'Dr Joy Sanyal ', 'drjoysanyal@gmail.com', 'Durgapur ', 'Healthworld ', NULL, NULL, '2021-04-18 19:42:21', '2021-04-18 19:42:21', '2021-04-18 21:12:21', 0, 'Abbott', '919dd81dd50cb5af55a3c2f5f165cdee'),
(101, 'Debdatta Majumdar	', 'debdattamajumdar76@gmail.com', 'Kolkata', 'RTIICS', NULL, NULL, '2021-04-18 19:42:58', '2021-04-18 19:42:58', '2021-04-18 21:12:58', 0, 'Abbott', 'e1ddf221bf959483f3c3c50c53418f35'),
(102, 'Babasaheb ', 'dbabasaheb3@gmail.com', 'Kolkata ', 'RN Tagore', NULL, NULL, '2021-04-18 19:43:21', '2021-04-18 19:43:21', '2021-04-18 21:13:21', 0, 'Abbott', '8f604ed3460ea3b1b9f5d70fc0adf02c'),
(103, 'Suman Kumar Saha', 'sumankumarsaha360@gmail.com', 'Kolkata', 'R.N.Tagore Hospital', NULL, NULL, '2021-04-18 19:44:12', '2021-04-18 19:44:12', '2021-04-18 21:14:12', 0, 'Abbott', 'ac1d3091a1f0ec4560d32c674d6c8b55'),
(104, 'K Ghosh Hazra	', 'drkghazra99@rediffmail.com', 'Kolkata', 'RTIICS', NULL, NULL, '2021-04-18 19:44:38', '2021-04-18 19:44:38', '2021-04-18 21:14:38', 0, 'Abbott', '1fa5a7a3ed11b9526ae3ab194bfab988'),
(105, 'Koushik Dasgupta	', 'koushik_nmc45@yahoo.com', 'Kolkata', 'RTIICS', NULL, NULL, '2021-04-18 19:45:29', '2021-04-18 19:45:29', '2021-04-18 21:15:29', 0, 'Abbott', '3311061ff943a12afa5b69443686f36b'),
(106, 'Dr Faisal', 'faisalburah@yahoo.com', 'Kolkata', 'SSKM', NULL, NULL, '2021-04-18 19:46:55', '2021-04-18 19:46:55', '2021-04-18 21:16:55', 0, 'Abbott', '392349f21468170a4037d97600b0241c'),
(107, 'Prithwiraj Bhattacharjee', 'prithwirajbhattacharjee88@gmail.com', 'Kolkata', 'Fortis Hospital', NULL, NULL, '2021-04-18 19:47:42', '2021-04-18 19:47:42', '2021-04-18 21:17:42', 0, 'Abbott', 'e5058ef8e3dccadb2312be2c29896b64'),
(108, 'Dr S Alam', 'sharwar2011@gmail.com', 'Kolkata', 'SSKM', NULL, NULL, '2021-04-18 19:49:54', '2021-04-18 19:49:54', '2021-04-18 21:19:54', 0, 'Abbott', '94424abeed2e090bf0bcaee1f2a92555'),
(109, 'Dr Tariq Ali', 'drmohdtariqali@gmail.com', 'Kolkata', 'R G Kar Hospital', NULL, NULL, '2021-04-18 19:51:30', '2021-04-18 19:51:30', '2021-04-18 21:21:30', 0, 'Abbott', '73cbc45b3d88232f97122d9ccb4daabc'),
(110, 'Dr Satish Ramteke', 'satish.ramteke@gmail.com', 'Kolkata', 'R G Kar Hospital', NULL, NULL, '2021-04-18 19:52:24', '2021-04-18 19:52:24', '2021-04-18 21:22:24', 0, 'Abbott', '0bd7622664f499f305ee3127358e8c5f'),
(111, 'Dr Tirthankar Roy ', 'helpfultiro@gmail.com', 'Kolkata', 'NRS Medical College ', NULL, NULL, '2021-04-18 19:53:13', '2021-04-18 19:53:13', '2021-04-18 21:23:13', 0, 'Abbott', '0a52a88c22c06c1b5abdb96734a799e8'),
(112, 'Dr Debjyoti Dutta ', 'drdebajyoti.nrs@gmail.com', 'Kolkata', 'NRS Medical College ', NULL, NULL, '2021-04-18 19:54:03', '2021-04-18 19:54:03', '2021-04-18 21:24:03', 0, 'Abbott', 'cd50738a3e391d997e1501d6ef26952d'),
(113, 'Dr Kalyan Saha', 'kalyan.sahadr@gmail.com', 'Kolkata', 'Calcutta Medical College ', NULL, NULL, '2021-04-18 19:54:35', '2021-04-18 19:54:35', '2021-04-18 21:24:35', 0, 'Abbott', 'ab3f0d7d8fd76f1f29fc5b1111efc9d8'),
(114, 'Dr Mani Pandey', 'manidmch06@gmail.com', 'Kolkata', 'SSKM', NULL, NULL, '2021-04-18 19:55:21', '2021-04-18 19:55:21', '2021-04-18 21:25:21', 0, 'Abbott', '5641db2f68cda3e005852cf05e2104d9'),
(115, 'Uttam Kumar Ojha', 'uttamojha.ctc@gmail.com', 'Bhubaneswar', 'Apollo hospitals', NULL, NULL, '2021-04-18 19:55:28', '2021-04-18 19:55:28', '2021-04-18 21:25:28', 0, 'Abbott', '52a5f855364c5eb5fa7b75cef29058d1'),
(116, 'Dr Rohit Shah', 'rohit.mbbs.md.dm@gmail.com', 'Kolkata', 'SSKM', NULL, NULL, '2021-04-18 19:55:38', '2021-04-18 19:55:38', '2021-04-18 21:25:38', 0, 'Abbott', 'b513ab8569802bb1b90d538212e83bd0'),
(117, 'Dr Ajinkya Moharkar ', 'ajinkyamoharkar1@gmail.com', 'Kolkata', 'SSKM', NULL, NULL, '2021-04-18 19:56:32', '2021-04-18 19:56:32', '2021-04-18 21:26:32', 0, 'Abbott', '86c6c22572f50d9f47f62f4705336bcc'),
(118, 'Priyam Mukherjee', 'drpriyam.mukherjee37@gmail.com', 'Kolkata', 'Medica Hospital', NULL, NULL, '2021-04-18 19:57:08', '2021-04-18 19:57:08', '2021-04-18 21:27:08', 0, 'Abbott', '9bb9eb250bed0c3a18f7028a823313e7'),
(119, 'Dr Ronita Saha ', 'neelrongpriyo@gmail.com', 'Kolkata', 'SSKM', NULL, NULL, '2021-04-18 19:57:36', '2021-04-18 19:57:36', '2021-04-18 21:27:36', 0, 'Abbott', '5477d0142618b8b1c1826509ee0072eb'),
(120, 'Suman Kumar Saha', 'sumankumarsaha360@gmail.com', 'Kolkata', 'R.N.Tagore Hospital', NULL, NULL, '2021-04-18 19:58:31', '2021-04-18 19:58:31', '2021-04-18 21:08:27', 0, 'Abbott', '3e8e692ae560968d385be9e852a4a051'),
(121, 'Dr Palden Bhutia', 'paldenbhutia869@gmail.com', 'Kolkata', 'SSKM', NULL, NULL, '2021-04-18 19:59:44', '2021-04-18 19:59:44', '2021-04-18 21:29:44', 0, 'Abbott', '80962256fc7831466ff5b56b10b05bc3'),
(122, 'Sanjib Mukherjee	', 'sanjaeevmshram2563@gmail.com', 'Kolkata', 'Medica Hospital', NULL, NULL, '2021-04-18 20:00:08', '2021-04-18 20:00:08', '2021-04-18 21:30:08', 0, 'Abbott', 'd798712ba819850163ce35c264fed297'),
(123, 'Sanjib Mukherjee	', 'sanjaeevmshram2563@gmail.com', 'Kolkata', 'Medica Hospital', NULL, NULL, '2021-04-18 20:00:08', '2021-04-18 20:00:08', '2021-04-18 21:30:08', 0, 'Abbott', '7ae6506031eb006f7244b0fafcf25acc'),
(124, 'Sanjib Mukherjee	', 'sanjaeevmshram2563@gmail.com', 'Kolkata', 'Medica Hospital', NULL, NULL, '2021-04-18 20:00:10', '2021-04-18 20:00:10', '2021-04-18 21:30:10', 0, 'Abbott', 'd371244cd29c7e4ae14311243f1c54cc'),
(125, 'Dr Sushant Patil ', 'dr.sushantpatil87@gmail.com', 'Kolkata', 'NRS Medical College ', NULL, NULL, '2021-04-18 20:00:33', '2021-04-18 20:00:33', '2021-04-18 21:30:33', 0, 'Abbott', '3be7f037e107e88121ffbde42ad4d990'),
(126, 'Snehil Goswami', 'snehil.1508@gmail.com', 'Raipur', 'NH MMI RAIPUR', NULL, NULL, '2021-04-18 20:02:17', '2021-04-18 20:02:17', '2021-04-18 21:32:17', 0, 'Abbott', '31a2a3b0626a8e07fb72b0e07cddf97b'),
(127, 'Achinta Bhattacharya	', 'achbhat67@gmail.com', 'Kolkata', 'VECC', NULL, NULL, '2021-04-18 20:02:34', '2021-04-18 20:02:34', '2021-04-18 21:32:34', 0, 'Abbott', '34221c20f475d28d038ce955e4de3c50'),
(128, 'Koushik', 'Koushikdas@gmaol.com', 'Kolkata ', 'Nh', NULL, NULL, '2021-04-18 20:05:03', '2021-04-18 20:05:03', '2021-04-18 21:35:03', 0, 'Abbott', '54bbfc82f853a7d3ec2a7d673522e09d'),
(129, 'Kalyan Goswami', 'Kalyan.goswami@abbott.com', 'Bangalore ', 'AV ', NULL, NULL, '2021-04-18 20:09:32', '2021-04-18 20:09:32', '2021-04-18 21:39:32', 0, 'Abbott', '5806389ef64ba0664bddaaad83eb3a9c'),
(130, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-04-18 20:37:16', '2021-04-18 20:37:16', '2021-04-18 21:07:24', 0, 'Abbott', '95a5c38ac3db17a9e2c05991b9e89433'),
(131, 'Pooja', 'test@test.com', 'Test', 'Test', NULL, NULL, '2021-04-18 20:38:06', '2021-04-18 20:38:06', '2021-04-18 20:38:28', 0, 'Abbott', 'd8f7c366cf108b1c30ffa5d33217c3ef'),
(132, 'Sujoy', 'sujoyfortis@gmail.com', 'Kolkata ', 'Fortis', NULL, NULL, '2021-04-18 20:57:24', '2021-04-18 20:57:24', '2021-04-18 21:06:39', 0, 'Abbott', '6904cbb965b4c6263fcaae6f6e3779ae'),
(133, 'Saurabh Dhumale', 'saurabhdhumale@icloud.com', 'Kolkata', 'RTIICS', NULL, NULL, '2021-04-18 21:11:01', '2021-04-18 21:11:01', '2021-04-18 22:41:01', 0, 'Abbott', 'b2b14c27cf9c086e344807abf90b967c'),
(134, 'dr sarat ku sahoo', 'drsaratsahoo@rediffmail.com', 'bbsr', 'sumum', NULL, NULL, '2021-04-21 10:31:00', '2021-04-21 10:31:00', '2021-04-21 12:01:00', 0, 'Abbott', 'd118cba19e6c9ca65ce30aa5121a6e33'),
(135, 'dr sarat ku sahoo', 'drsaratsahoo@rediffmail.com', 'bbsr', 'sumum', NULL, NULL, '2021-04-21 10:31:40', '2021-04-21 10:31:40', '2021-04-21 12:01:40', 0, 'Abbott', '76150ca6953de812595d820b19bf79e6');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculty_new`
--
ALTER TABLE `faculty_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `faculty_new`
--
ALTER TABLE `faculty_new`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
